package honcharuk.julia.springboot.repository;

import honcharuk.julia.springboot.service.CompilationTimeData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CompilationTimeRepository extends JpaRepository<CompilationTimeData, Long> {
    @Query("SELECT DISTINCT ctd.project FROM CompilationTimeData ctd")
    List<String> findAllProjectNames();

    @Query("SELECT DISTINCT ctd.result FROM CompilationTimeData ctd WHERE ctd.result != 'success'")
    List<String> findAllFailedResults();

    List<CompilationTimeData> findByCompilationTimeGreaterThanEqual(int compilationTime);

    List<CompilationTimeData> findByCompilationTimeLessThan(int compilationTime);

    List<CompilationTimeData> findByResult(String result);
}

