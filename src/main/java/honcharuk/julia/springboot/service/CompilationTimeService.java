package honcharuk.julia.springboot.service;

import honcharuk.julia.springboot.repository.CompilationTimeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompilationTimeService {

    private CompilationTimeRepository compilationTimeRepository;


    @Autowired
    public CompilationTimeService(CompilationTimeRepository compilationTimeRepository) {
        this.compilationTimeRepository = compilationTimeRepository;
    }

    public void addOrUpdateCompilationTime(CompilationTimeData compilationTimeData) {
        compilationTimeRepository.save(compilationTimeData);
    }

    public List<String> getProjectNames() {
        return compilationTimeRepository.findAllProjectNames();
    }

    public List<String> getFailedResults() {
        return compilationTimeRepository.findAllFailedResults();
    }

    public List<CompilationTimeData> getLongCompilations() {
        return compilationTimeRepository.findByCompilationTimeGreaterThanEqual(1000);
    }

    public List<CompilationTimeData> getNormalCompilations() {
        return compilationTimeRepository.findByCompilationTimeLessThan(1000);
    }

    public List<CompilationTimeData> getSuccessResults() {
        return compilationTimeRepository.findByResult("success");
    }
}

