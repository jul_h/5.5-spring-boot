package honcharuk.julia.springboot.controller;

import honcharuk.julia.springboot.service.CompilationTimeData;
import honcharuk.julia.springboot.service.CompilationTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Controller
public class CompilationTimeController {
    private CompilationTimeService compilationTimeService;

    @Autowired
    public CompilationTimeController(CompilationTimeService compilationTimeService) {
        this.compilationTimeService = compilationTimeService;
    }

    @PostMapping("/register-compilation-time")
    public ResponseEntity<String> addOrUpdateCompilationTime(@RequestBody CompilationTimeData compilationTimeData) {
        try {
            compilationTimeService.addOrUpdateCompilationTime(compilationTimeData);
            return ResponseEntity.status(HttpStatus.CREATED).body("Registration Successful");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Registration Failed");
        }
    }

    @GetMapping("/projects")
    public List<String> getProjectNames() {
        return compilationTimeService.getProjectNames();
    }

    @GetMapping("/failed-results")
    public List<String> getFailedResults() {
        return compilationTimeService.getFailedResults();
    }

    @GetMapping("/long-compilation")
    public List<CompilationTimeData> getLongCompilations() {
        return compilationTimeService.getLongCompilations();
    }

    @GetMapping("/normal-compilation")
    public List<CompilationTimeData> getNormalCompilations() {
        return compilationTimeService.getNormalCompilations();
    }

    @GetMapping("/success-result")
    public List<CompilationTimeData> getSuccessResults() {
        return compilationTimeService.getSuccessResults();
    }
}

